# TRIC
Welcome to the repository of TRIC :smile: - Transformer-based Relatice Image Captioner which was created as a part of my Master Thesis which is about Relative Captioning. Relative Captioning is similar to Image Captioning. In the Image Captioning task, given the image the model's task is to generate caption or catpions describing the image. In Relative Image Captioning task, given 2 images, the candidate and the target, the model is generating a caption describing relative differences between those two with respect to the target image - probably the best way to comprahend it is to look at the image below.

<div align="center">
<img src="TRIC_img.png" width="450" height="225" >
<p>Example of Relative Captioning where the bottom image is the target one and the top one is the candidate</p>
</div>

More information about this task and the model overall can be found in the blog post here -> :robot: [TRIC BLOG POST, click it, do it](https://medium.com/p/d0edac40e5df/edit) :robot:
### Repository structure
* [notebooks/](https://gitlab.com/user-modeling/transformer-based-user-modeling/-/tree/master/notebooks) -> notebooks presenting subsequent milestones of the project. `TRiC_MODEL_DEMO.ipynb` contains demo of the trained relative captioning model (see section **Model demo**)
* [scripts/train_cloud.sh](https://gitlab.com/user-modeling/transformer-based-user-modeling/-/blob/master/scripts/train-cloud.sh) -> script that submits training job on Google Cloud Platform assuming that in GCP bucket appropriate data is stored (see **Model demo** first subsection)
* [trainer/](https://gitlab.com/user-modeling/transformer-based-user-modeling/-/tree/master/trainer) - contains project codebase. The best place to start exploring it is the [experiment.py](https://gitlab.com/user-modeling/transformer-based-user-modeling/-/blob/master/trainer/experiment.py) file. It presents whole training process of the TRIC model.
### Model demo
The notebook `./notebook/TRIC_MODEL_DEMO.ipynb` contains an example of trained model generating captions for pairs of images, In order to see its performance on training and validation sets, please perform the following step:

1. Replace the empty `/data` folder with actual data -> [link](https://drive.google.com/drive/folders/17Zd4hfuX6ugOf4rz-dQL8c37Ps5idz5A?usp=sharing) and replace the empty 
2. Install Conda and then install all needed packages from `transformer-based-user-modeling.yml`:
- Conda installation -> [link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)
- Create conda env from `transformer-based-user-modeling.yml` -> `conda env create -f transformer-based-user-modeling.yml`
- Activate enviroment -> `conda activate transformer-based-user-modeling`
3. Run Jupyter Lab by typing in terminal -> `jupyter lab`
4. Go to `./notebooks/TRIC_MODEL_DEMO.ipynb`
5. Run all cells in it -> click "Run" and then "Run All Cells"
6. At the end of the notebook under `Producing example caption` section an example can be visible
7. In order to generate caption for another pair of images change `IMAGE_PAIR_ID` to some other index from 0 to 511 and run all cells from section `Choose pair ID from 0 to 511` again.
