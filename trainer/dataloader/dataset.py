import torch
from torch.utils.data import Dataset

from trainer.utils.helpers import load_image
from pathlib import Path


class RelativeCaptioningDataset(Dataset):
    """Dataset class for Relative image captioning problem"""

    def __init__(self, list_ids, images_dir, transform=None, client=None):
        # Initialization
        self.list_ids = list_ids
        self.transform = transform
        self.images_dir = Path(images_dir)
        self.client = client
        self.data = self._load_images()

    def __len__(self):
        # Denotes the total number of samples
        return len(self.list_ids)

    def __getitem__(self, index):

        # Generates one sample of data
        target_img, cand_img, caption = self.data[index]

        return target_img, cand_img, caption

    def _load_images(self):
        data = []
        for i, triple in enumerate(self.list_ids):

            # Get 2 images and 1 caption
            target_img_path = self.images_dir.joinpath(triple['target'] + ".jpg")
            candidate_img_path = self.images_dir.joinpath(triple['candidate'] + ".jpg")
            caption = triple['caption']

            # Load images
            target_img = load_image(path=target_img_path, client=self.client, is_rgb=True)
            candidate_img = load_image(path=candidate_img_path, client=self.client, is_rgb=True)

            # Apply transforms if specified
            if self.transform:
                target_img = self.transform(target_img)
                candidate_img = self.transform(candidate_img)

            # Stack images in a pair
            #target_cand_pair_images = torch.stack([target_img, candidate_img], 0)

            # Add captions and images to list
            data.append((target_img, candidate_img, caption))

            # Log every 1000 samples
            if i % 1000 == 0:
                print(f"{i}/{self.__len__()} samples loaded")

        print("All samples loaded successfully")
        return data


class MyCollator(object):
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        # self.device = device

    def __call__(self, batch):
        target_imgs, cand_imgs, captions = zip(*batch)

        # Merge images
        # target_batch = torch.stack(target_imgs, 0).to(self.device)
        # cand_batch = torch.stack(cand_imgs, 0).to(self.device)
        target_batch = torch.stack(target_imgs, 0)
        cand_batch = torch.stack(cand_imgs, 0)

        # Get true lengths of captions along with max batch sequence len
        cap_lengths = [len(self.tokenizer.tokenize(caption)) for caption in captions]
        max_batch_len = max(cap_lengths)

        # Get tokens indices and pad them to max_batch_len
        # Outputs Torch.tensor of size (batch_size, max_batch_len)
        encoded_dict = self.tokenizer(captions,
                                      add_special_tokens=True,
                                      truncation=True,
                                      max_length=max_batch_len,
                                      padding='max_length',
                                      return_tensors='pt')

        # Get tokens' indices
        # tokens_indices = encoded_dict['input_ids'].to(self.device)
        tokens_indices = encoded_dict['input_ids']

        # Segment ids for BERT -> all ones because we are encoding one sentence at a time
        # segments_ids = torch.ones_like(tokens_indices).to(self.device)
        segments_ids = torch.ones_like(tokens_indices)

        return target_batch, cand_batch, tokens_indices, segments_ids, cap_lengths

