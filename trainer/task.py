# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the \"License\");
# you may not use this file except in compliance with the License.\n",
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an \"AS IS\" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse

from trainer import experiment


def get_args():
    """Define the task arguments with the default values.

    Returns:
        experiment parameters
    """
    args_parser = argparse.ArgumentParser()

    # Global arguments
    args_parser.add_argument(
        '--seed',
        help='Random seed (default: 8)',
        type=int,
        default=8,
    )

    # Dataloader arguments
    args_parser.add_argument(
        '--batch-size',
        help='Batch size for each training and evaluation step.',
        type=int,
        default=100)
    args_parser.add_argument(
        '--shuffle-train',
        help='Whether to randomly pick data while creating batches ',
        action='store_true')
    args_parser.add_argument(
        '--num-workers',
        help='Number of workers',
        type=int,
        default=0)

    # Model arguments
    # Image Encoder
    args_parser.add_argument(
        '--enc-layers',
        help='Number of layers in Transformer Image Encoder',
        type=int,
        default=2)
    args_parser.add_argument(
        '--enc-heads',
        help='Number of heads in Transformer Image Encoder',
        type=int,
        default=4)
    args_parser.add_argument(
        '--enc-pos-ff-dim',
        help='Dimensions of Positional Fully-conected layers in Transformer Encoder',
        type=int,
        default=3072)
    args_parser.add_argument(
        '--enc-dropout',
        help='Dropout in Transformer Image Encoder',
        type=float,
        default=0.1)

    # Captions Decoder
    args_parser.add_argument(
        '--dec-layers',
        help='Number of layers in Transformer Captions Decoder',
        type=int,
        default=2)
    args_parser.add_argument(
        '--dec-heads',
        help='Number of heads in Transformer Captions Decoder',
        type=int,
        default=4)
    args_parser.add_argument(
        '--dec-pos-ff-dim',
        help='Dimensions of Positional Fully-conected layers in Transformer Captions Decoder',
        type=int,
        default=3072)
    args_parser.add_argument(
        '--dec-dropout',
        help='Dropout in Transformer Captions Decoder',
        type=float,
        default=0.1)

    # BERT embeddings
    args_parser.add_argument(
        '--bert-last-n-layers',
        help='Last n layers of BERT model to create embeddings for words',
        type=int,
        default=2)

    # Max length of the input caption
    args_parser.add_argument(
        '--cap-max-len',
        help='Maximum length of the input sequence to te TRIC Decoder',
        type=int,
        default=21)

    # Image model
    args_parser.add_argument(
        '--img-model-out-dim',
        help='Out dimension of image model: ResNet etc.',
        type=int,
        default=2048)

    # Training arguments
    args_parser.add_argument(
        '--num-epochs',
        help="""\
        Maximum number of training data epochs on which to train.
        If both --train-size and --num-epochs are specified,
        --train-steps will be: (train-size/train-batch-size) * num-epochs.\
        """,
        default=50,
        type=int,
    )
    args_parser.add_argument(
        '--learning-rate',
        help='Learning rate value for the optimizers.',
        default=0.0001,
        type=float)
    args_parser.add_argument(
        '--label-smoothing',
        help='Label smoothing parameter for Cross Entropy',
        default=0.1,
        type=float)
    args_parser.add_argument(
        '--clip',
        help='Gradient clippling',
        default=1,
        type=float)
    args_parser.add_argument(
        '--weight-decay',
        help="""
          The factor by which the learning rate should decay by the end of the
          training.

          decayed_learning_rate =
            learning_rate * decay_rate ^ (global_step / decay_steps)

          If set to 0 (default), then no decay will occur.
          If set to 0.5, then the learning rate should reach 0.5 of its original
              value at the end of the training.
          Note that decay_steps is set to train_steps.
          """,
        default=0,
        type=float)

    # Saved model arguments
    args_parser.add_argument(
        '--job-dir',
        help='GCS location to export models')
    args_parser.add_argument(
        '--model-name',
        help='The name of your saved model',
        default='model.pth')

    return args_parser.parse_args()


def main():
    """Setup / Start the experiment
    """
    args = get_args()
    experiment.run(args)


if __name__ == '__main__':
    main()
