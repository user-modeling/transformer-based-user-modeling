#!/usr/bin/env python
# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import string
from trainer.utils.helpers import is_any_img_not_exist, is_any_img_blacknwhite
from google.cloud.storage import Client

client = Client()

# Load and preprocess data
BUCKET_NAME = "tric_bucket"
FILTERS = [is_any_img_not_exist, is_any_img_blacknwhite]
IMAGES_TAR_FILE_BUCKET_PATH = "data/images/dresses.tar.gz"
IMAGES_TAR_FILE_LOCAL_PATH = "./tst_act.tar.gz"
EXTRACTION_DEST_LOCAL_FOLDER = "./"
TRAIN_CAPTION_META_PATH = f"gs://{BUCKET_NAME}/data/images_metadata/cap_dress_train.json"
TRAIN_EXPLODED_SAVE_PATH = f"gs://{BUCKET_NAME}/data/images_metadata/cap_dress_train_exploded.jsonl"
VAL_CAPTION_META_PATH = f"gs://{BUCKET_NAME}/data/images_metadata/cap_dress_val.json"
VAL_EXPLODED_SAVE_PATH = f"gs://{BUCKET_NAME}/data/images_metadata/cap_dress_val_exploded.jsonl"

# Constants
REPLACE_DICT = {"3.4": "3/4", "&": "and"}
PUNCT = string.punctuation.replace(" ", "").replace(".", "").replace("-", "").replace("/", "").replace("&", "").replace(
    "'", "")
MANUAL_SPELL_MAP = {'sleves': 'sleeves',
                    'sleevs': 'sleeves',
                    'abd': 'and',
                    'gret': 'green',
                    'longAND': 'long and',
                    'highneck': 'high-neck',
                    'sirt': 'skirt',
                    'tanktop': 'tanktop',
                    'tigher': 'tighter',
                    'scoup': 'scoupe',
                    'ligher': 'lighter',
                    'sleevless': 'sleeveless',
                    'wasit': 'waist',
                    'lither': 'lighter',
                    'atire': 'attire',
                    'strappier': 'strappier',
                    'sleeving': 'sleeving',
                    'blet': 'belt',
                    'minidress': 'minidress',
                    'shortsleeve': 'short-sleeve',
                    'shader': 'shaded',
                    'clored': 'colored',
                    'fired': 'fited',
                    'shoter': 'shorter',
                    'lacier': 'lacier',
                    'disered': 'desired',
                    'colorwise': 'colorwise',
                    'offwhite': 'off-white',
                    'shirtdress': 'shirt dress',
                    'beltess': 'beltless',
                    'seethrough': 'see-through',
                    'rouching': 'rouching',
                    'v-neck': 'v-neck',
                    'vneck': 'v-neck',
                    'sheerer': 'sheerer',
                    'darke': 'darker',
                    'snall': 'small',
                    'stribe': 'stripe',
                    'blackdress': 'black dress',
                    'texured': 'textured',
                    'torquose': 'turquoise',
                    'floorlenght': 'floor-lenght',
                    'belr': 'belt',
                    'sraps': 'straps',
                    'edy': 'edgy',
                    'stapless': 'strapless',
                    'Vneck': 'v-neck',
                    'puffier': 'puffier',
                    'losser': 'looser',
                    'pinkand': 'pink and',
                    'haltered': 'haltered',
                    'rouched': 'rouched',
                    'cowlneck': 'cowl neck',
                    'haler': 'halter',
                    'skeevs': 'sleeves',
                    'pattered': 'patterned'}
