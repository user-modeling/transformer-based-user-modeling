# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the \"License\");
# you may not use this file except in compliance with the License.\n",
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an \"AS IS\" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import torch
import torch.nn as nn
from torch.optim import Adam
import torchvision.models as models
from transformers import BertModel

from trainer.embeddings.embed import BERTEmbedding
from trainer.model.res_net import ResNetEmbedding
from trainer.model.image_encoder import ImageEncoder
from trainer.model.captions_decoder import CaptionsDecoder
from trainer.model.images_to_captions import Images2Captions
from trainer.utils.helpers import initialize_weights, count_parameters
from trainer.evaluation.custom_losses import CrossEntropyLossSmooth


def create(args, device, vocab):
    """
    Create the model, loss function, and optimizer to be used for the DNN

    Args:
      args: experiment parameters.
      device: cpu or gpu
      vocab: Vocabulary object created from train and val dataset
    """

    # Get pre-trained BERT for captions embeddings
    print("======= Creating BERT model =======")
    pretrained_bert = BertModel.from_pretrained(pretrained_model_name_or_path='bert-base-uncased',
                                                output_hidden_states=True)
    _ = pretrained_bert.eval()
    bert_model = BERTEmbedding(bert_model=pretrained_bert, n_last_layers=args.bert_last_n_layers)
    hid_dim = bert_model.final_dim

    # Get pre-trained ResNet for images embeddings
    print("======= Creating Image Model model =======")
    resnet_model = models.resnet101(pretrained=True)
    resnet_embedding = ResNetEmbedding(resnet=resnet_model,
                                       out_resnet=args.img_model_out_dim,
                                       hid_dim=hid_dim)

    # Get Image Encoder model
    print("======= Creating Image Encoder model =======")
    encoder_model = ImageEncoder(embedding_net=resnet_embedding,
                                 hid_dim=hid_dim,
                                 n_layers=args.enc_layers,
                                 n_heads=args.enc_heads,
                                 pf_dim=args.enc_pos_ff_dim,
                                 dropout=args.enc_dropout)

    # Get Captions Decoder model
    print("======= Creating Captions Decoder model =======")
    vocab_len = len(vocab)
    decoder_model = CaptionsDecoder(embedding_net=bert_model,
                                    hid_dim=hid_dim,
                                    output_dim=vocab_len,
                                    n_layers=args.dec_layers,
                                    n_heads=args.dec_heads,
                                    pf_dim=args.dec_pos_ff_dim,
                                    dropout=args.dec_dropout,
                                    max_length=args.cap_max_len)

    # Get Images 2 Captions model
    print("======= Assembling TRIC model =======")
    tric_model = Images2Captions(encoder=encoder_model,
                                 decoder=decoder_model,
                                 trg_pad_idx=vocab.ids_to_ids_adj[vocab.tokens_to_ids['[PAD]']])

    # Initialzie weights
    _ = tric_model.apply(initialize_weights)

    # Print number of trainable parameters
    print(f'The model has {count_parameters(tric_model):,} trainable parameters')

    # Set up optimizer and loss function
    print("======= Setting up optimizer and loss function =======")
    optimizer = Adam(tric_model.parameters(),
                     lr=args.learning_rate,
                     betas=(0.9, 0.98))
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer=optimizer,
                                                           mode='min',
                                                           factor=0.8,
                                                           patience=20,
                                                           verbose=True)
    trg_pad_idx = vocab.ids_to_ids_adj[vocab.tokens_to_ids['[PAD]']]
    #criterion = nn.CrossEntropyLoss(ignore_index=trg_pad_idx)
    criterion = CrossEntropyLossSmooth(ignore_index=trg_pad_idx, smooth_eps=args.label_smoothing)

    return tric_model, criterion, optimizer, scheduler
