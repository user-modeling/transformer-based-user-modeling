import math
import torch
import torch.nn as nn


class BERTEmbedding(nn.Module):
    def __init__(self, bert_model, n_last_layers, out_dim=768):
        """Extracts embeddings for a batch of captions based on sum of n_last_layers"""
        super().__init__()
        self.out_dim = out_dim
        self.n_last_layers = n_last_layers
        self.final_dim = out_dim
        self.bert_model = bert_model.eval()

        # n last layers used to produce final embedding
        self.n_last_layers = n_last_layers

        for param in self.bert_model.parameters():
            param.requires_grad = False

    def forward(self, captions_ids, segments_ids):
        # Get batch shape
        batch_size, seq_len = captions_ids.shape

        # Pass captions ids with segment ids through BERT
        outputs = self.bert_model(captions_ids, segments_ids)

        # Get hidden states
        hidden_states = outputs[2]

        # Concatenate the tensors for all layers.
        tokens_embeddings = torch.stack(hidden_states, dim=0)

        # Swap dimensions, so we get tensors in format: [sentence, tokens, hidden layes, features]
        tokens_embeddings = tokens_embeddings.permute(1, 2, 0, 3)

        # Use sum of last n hidden layers to extract embeddings
        embeddings = torch.sum(tokens_embeddings[:, :, -self.n_last_layers:, :], dim=2)

        return embeddings


# Source: https://pytorch.org/tutorials/beginner/transformer_tutorial.html
class PositionalEncoding(nn.Module):

    def __init__(self, hid_dim, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, hid_dim)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, hid_dim, 2).float() * (-math.log(10000.0) / hid_dim))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:, :x.size(1)]
        return x
