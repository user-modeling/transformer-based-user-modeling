import json
import tarfile
import random
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from smart_open import open
from pathlib import Path
from google.cloud.exceptions import NotFound
from google.cloud import storage


def does_path_exist(path, client=None):
    try:
        with open(path, mode='rb', transport_params=dict(client=client)) as binary:
            return True
    except (NotFound, FileNotFoundError) as e:
        return False


def is_any_img_not_exist(trg_path, cand_path, client=None):
    return not does_path_exist(trg_path, client) or not does_path_exist(cand_path, client)


def is_any_img_blacknwhite(trg_path, cand_path, client):

    # Load images
    trg = load_image(trg_path, client)
    cand = load_image(cand_path, client)

    # Check whether either of them is 2-dim -> black and white image
    return len(trg.shape) == 2 or len(cand.shape) == 2


def append_to_jsonl(path, data, client=None):
    with open(path, mode='a', transport_params=dict(client=client)) as f:
        for line in data:
            json_record = json.dumps(line)
            f.write(json_record + '\n')


def load_jsonl(path, client=None):
    jsonl_list = []
    try:
        with open(path, mode='r', transport_params=dict(client=client)) as file:
            for line in file:
                jsonl_list.append(json.loads(line))
    except (FileNotFoundError, NotFound) as e:
        return []

    return jsonl_list


def load_json(path, client=None):
    with open(path, mode='r', transport_params=dict(client=client)) as file:
        json_file = json.load(file)
    return json_file


def load_image(path, client=None, is_rgb=False):
    with open(path, mode='rb', transport_params=dict(client=client)) as img_str:
        if is_rgb:
            img = np.array(Image.open(img_str).convert('RGB'))
        else:
            img = np.array(Image.open(img_str))
    return img


def download_bucket_file(bucket_path, local_path, bucket_name):
    # Initialise a client
    storage_client = storage.Client()

    # Create a bucket object for our bucket
    bucket = storage_client.get_bucket(bucket_name)

    # Create a blob object from the filepath
    blob = bucket.blob(bucket_path)

    # Download the file to a destination
    blob.download_to_filename(local_path)


def extract_tar_file(path, dest_folder):
    my_tar = tarfile.open(path)
    my_tar.extractall(dest_folder)
    my_tar.close()
    return Path(dest_folder).joinpath("dresses")


def get_params(config_path="../trainer/config/params.json", client=None):
    with open(config_path, mode='r', transport_params=dict(client=client)) as jsonfile:
        params = json.load(jsonfile)

    return params


def set_seed(seed):    
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def initialize_weights(model):
    if hasattr(model, 'weight') and model.weight.dim() > 1:
        nn.init.xavier_uniform_(model.weight.data)
