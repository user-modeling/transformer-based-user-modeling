import torch
import torch.nn as nn
# source - https://charon.me/posts/pytorch/pytorch_seq2seq_6/


class Images2Captions(nn.Module):
    def __init__(self,
                 encoder,
                 decoder,
                 trg_pad_idx):
        super().__init__()

        self.encoder = encoder
        self.decoder = decoder
        self.trg_pad_idx = trg_pad_idx

    def make_trg_mask(self, trg, device):
        # trg = [batch size, trg len]

        trg_pad_mask = (trg != self.trg_pad_idx).unsqueeze(1).unsqueeze(2)
        trg_pad_mask = trg_pad_mask.to(device)

        # trg_pad_mask = [batch size, 1, 1, trg len]

        trg_len = trg.shape[1]

        trg_sub_mask = torch.tril(torch.ones((trg_len, trg_len), device=device)).bool()
        trg_sub_mask = trg_sub_mask.to(device)

        # trg_sub_mask = [trg len, trg len]

        trg_mask = trg_pad_mask & trg_sub_mask

        # trg_mask = [batch size, 1, trg len, trg len]

        return trg_mask

    def forward(self, src, trg, trg_segments_ids):
        # src_target, src_candidate = (batch_target, batch_candidate)
        # src_target = [batch_size]
        # src_candidate = [batch_size]
        # trg = [batch size, trg len]

        trg_mask = self.make_trg_mask(trg, trg.device)

        # src_mask = [batch size, 1, 1, src len]
        # trg_mask = [batch size, 1, trg len, trg len]

        enc_src = self.encoder(src)

        # enc_src = [batch size, src len, hid dim]

        output, attention = self.decoder(trg, trg_segments_ids, trg_mask, enc_src)

        # output = [batch size, trg len, output dim]
        # attention = [batch size, n heads, trg len, src len]

        return output, attention
