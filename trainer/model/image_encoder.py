import torch
import torch.nn as nn

from trainer.model.position_wise_feed_forward import PositionwiseFeedforwardLayer
from trainer.model.attention_layers import MultiHeadAttentionLayer
# source - https://charon.me/posts/pytorch/pytorch_seq2seq_6/


class ImageEncoder(nn.Module):
    def __init__(self,
                 embedding_net,
                 hid_dim,
                 n_layers,
                 n_heads,
                 pf_dim,
                 dropout):
        super().__init__()

        self.hid_dim = hid_dim
        self.img_embedding = embedding_net

        self.layers = nn.ModuleList([EncoderLayer(hid_dim,
                                                  n_heads,
                                                  pf_dim,
                                                  dropout)
                                     for _ in range(n_layers)])

        self.dropout = nn.Dropout(dropout)

    def forward(self, src, src_mask=None):
        # The src_mask will be None because we are not padding anything in images
        # We are feeding the Encoder with 588 feature vectors of size 768: 196x768 dim for each of the following:
        # img1, img2, img1*img2 -> concatenation -> 196*3=588x768
        # tartget_batch = [batch_size, n_channels, width, height]
        # candidate_batch = [batch_size, n_channels, width, height]

        target_batch, candidate_batch = src
        batch_size, n_channels, width, height = target_batch.shape

        # [batch_size, hid_dim, H, W] - H=W=14 when resnet101 and input 3x224x224
        trg = self.img_embedding(target_batch)
        cand = self.img_embedding(candidate_batch)

        # [batch_size, hid_dim, H*W] - H*W=196 when resnet101 and input 3x224x224
        trg = torch.reshape(trg, (batch_size, self.hid_dim, -1))
        cand = torch.reshape(cand, (batch_size, self.hid_dim, -1))

        # [batch_size, H*W, hid_dim] - H*W=196 when resnet101 and input 3x224x224
        trg = trg.transpose(1, 2)
        cand = cand.transpose(1, 2)
        trg_cand_mul = trg * cand

        # [batch_size, H*W*3, hid_dim] - H*W*3=588 when resnet101 and input 3x224x224
        # src = torch.cat((trg, cand, trg_cand_mul), 1)
        # DECREASE SEQ LEN
        # src = torch.cat((trg, cand), 1)
        # src = self.dropout(torch.cat((trg, cand, trg_cand_mul), 1))
        src = trg_cand_mul

        for i, layer in enumerate(self.layers):
            src = layer(src, src_mask)

        # src = # [batch_size, H*W*3, hid_dim] - H*W*3=588 when resnet101 and input 3x224x224

        return src


class EncoderLayer(nn.Module):
    def __init__(self,
                 hid_dim,
                 n_heads,
                 pf_dim,
                 dropout):
        super().__init__()

        self.self_attn_layer_norm = nn.LayerNorm(hid_dim)
        self.ff_layer_norm = nn.LayerNorm(hid_dim)
        self.self_attention = MultiHeadAttentionLayer(hid_dim, n_heads, dropout)
        self.positionwise_feedforward = PositionwiseFeedforwardLayer(hid_dim,
                                                                     pf_dim,
                                                                     dropout)
        self.dropout = nn.Dropout(dropout)

    def forward(self, src, src_mask):
        # src = [batch size, src len, hid dim]
        # src_mask = [batch size, src len]

        # self attention
        _src, _ = self.self_attention(src, src, src, src_mask)  # multi-head( query, key, value, mask = None)

        # dropout, residual connection and layer norm
        src = self.self_attn_layer_norm(src + self.dropout(_src))

        # src = [batch size, src len, hid dim]

        # positionwise feedforward
        _src = self.positionwise_feedforward(src)

        # dropout, residual and layer norm
        src = self.ff_layer_norm(src + self.dropout(_src))

        # src = [batch size, src len, hid dim]

        return src
