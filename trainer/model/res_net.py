import torch
import torch.nn as nn


class ResNetEmbedding(nn.Module):
    def __init__(self, resnet: nn.Module, out_resnet: int, hid_dim: int):
        """
        Wrapper around ResNet networks. It takes ResNet backbone and places fully connected layer on top of it
        in order to adjust ResNet output to hidden dim of Transformer. ResNet backbone is completely frozen and
        adjust layer is trainable.
        """
        super().__init__()
        self.resnet = resnet.eval()
        self.resnet = torch.nn.Sequential(*(list(self.resnet.children())[:-3]))
        self.resnet.requires_grad_(False)

        self.conv1D_adjust = torch.nn.Conv2d(out_resnet, hid_dim, 1)

    def forward(self, x: torch.Tensor):
        return self.conv1D_adjust(self.resnet(x))
