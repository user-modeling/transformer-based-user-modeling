# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the \"License\");
# you may not use this file except in compliance with the License.\n",
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an \"AS IS\" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time
import torch
import torch.nn as nn
from trainer import inputs
from trainer.model_builder import create
from trainer.training.train import train, evaluate, epoch_time


def run(args):
    """Load the data, train, evaluate, and export the model for serving and
     evaluating.

    Args:
      args: experiment parameters.
    """
    cuda_availability = torch.cuda.is_available()
    if cuda_availability:
        device = torch.device('cuda:{}'.format(torch.cuda.current_device()))
    else:
        device = 'cpu'
    print('\n*************************')
    print('`cuda` available: {}'.format(cuda_availability))
    print('Current Device: {}'.format(device))
    print("Let's use", torch.cuda.device_count(), "GPUs!")
    print('*************************\n')

    torch.manual_seed(args.seed)

    # Open our dataset
    print("============================ PREPARING DATA ============================\n")
    train_loader, val_loader, vocab = inputs.load_data(args, device)

    # Create the model, loss function, and optimizer
    print("============================ CREATING MODEL ============================\n")
    tric_model, criterion, optimizer, scheduler = create(args, device, vocab)

    # Enable Data Parallelism- multi GPU training
    if torch.cuda.device_count() > 1:
        tric_model = nn.DataParallel(tric_model).to(device)

    # Train / Test the model
    print("============================ TRAINING MODEL ============================\n")
    for epoch in range(1, args.num_epochs + 1):
        start_time = time.time()
        train_loss = train(tric_model, train_loader, optimizer, criterion, vocab, epoch, args, device)
        eval_loss = evaluate(tric_model, val_loader, criterion, vocab, epoch, device)
        scheduler.step(train_loss)
        end_time = time.time()
        epoch_mins, epoch_secs = epoch_time(start_time, end_time)
        print(f'Epoch: {epoch:02} | Time: {epoch_mins}m {epoch_secs}s')

    # Evaluate the model
    #print("Evaluate the model using the evaluation dataset")
    #test(sequential_model, eval_loader, criterion,
    #     args.num_epochs, report_metric=False)

    # Export the trained model
    torch.save(tric_model.state_dict(), args.model_name)

    # Save the model to GCS
    print("============================ SAVING TRAINED MODEL ============================\n")
    if args.job_dir:
        inputs.save_model(args)

    print("============================ TRAINED MODEL SAVED ============================\n")