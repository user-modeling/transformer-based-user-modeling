import torch


def train(model, iterator, optimizer, criterion, vocab, epoch, args, device):

    model.train()
    epoch_loss = 0

    for i, batch in enumerate(iterator):
        target_batch, cand_batch, captions_batch, segments_ids_batch, cap_lengths = batch

        # Move data to appropriate device
        target_batch = target_batch.to(device)
        cand_batch = cand_batch.to(device)
        captions_batch = captions_batch.to(device)
        segments_ids_batch = segments_ids_batch.to(device)

        optimizer.zero_grad()

        output, _ = model(src=(target_batch, cand_batch),
                          trg=captions_batch[:, :-1],
                          trg_segments_ids=segments_ids_batch[:, :-1])

        output_dim = output.shape[-1]

        output = output.contiguous().view(-1, output_dim)  # output = [batch size * trg len - 1, output dim]
        captions_batch = captions_batch[:, 1:].contiguous().view(-1)  # captions_batch = [batch size * trg len - 1]
        captions_batch = vocab.map_to_trainset_ids(bert_indices_tensor=captions_batch)
        output, captions_batch = output.to(device), captions_batch.to(device)

        loss = criterion(output, captions_batch)

        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)

        optimizer.step()

        epoch_loss += loss.item()
    # TODO: BLEU calculation - maybe outside of this function
    #bleu_score = calculate_bleu(model, iterator, vocab)
    print(f"[TRAIN], epoch: {epoch}, loss: {epoch_loss / len(iterator)}")
    return epoch_loss / len(iterator)


def evaluate(model, iterator, criterion, vocab, epoch, device):
    model.eval()

    epoch_loss = 0

    with torch.no_grad():
        for i, batch in enumerate(iterator):
            target_batch, cand_batch, captions_batch, segments_ids_batch, cap_lengths = batch

            # Move data to appropriate device
            target_batch = target_batch.to(device)
            cand_batch = cand_batch.to(device)
            captions_batch = captions_batch.to(device)
            segments_ids_batch = segments_ids_batch.to(device)

            output, _ = model(src=(target_batch, cand_batch),
                              trg=captions_batch[:, :-1],
                              trg_segments_ids=segments_ids_batch[:, :-1])

            output_dim = output.shape[-1]

            output = output.contiguous().view(-1, output_dim)  # output = [batch size * trg len - 1, output dim]
            captions_batch = captions_batch[:, 1:].contiguous().view(-1)  # trg = [batch size * trg len - 1]
            captions_batch = vocab.map_to_trainset_ids(bert_indices_tensor=captions_batch)
            output, captions_batch = output.to(device), captions_batch.to(device)
            loss = criterion(output, captions_batch)

            epoch_loss += loss.item()

    print(f"[EVAL], epoch: {epoch}, loss: {epoch_loss / len(iterator)}")
    return epoch_loss / len(iterator)


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs
