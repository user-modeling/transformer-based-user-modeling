# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the \"License\");
# you may not use this file except in compliance with the License.\n",
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an \"AS IS\" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime

from torchvision import transforms
from torch.utils.data import DataLoader

from transformers import BertTokenizer

from trainer.metadata import *
from trainer.preprocessing.preprocess import PreprocessData, CleanTextData
from trainer.dataloader.dataset import RelativeCaptioningDataset, MyCollator
from trainer.vocabulary.vocab import Vocab
from trainer.utils.helpers import download_bucket_file, extract_tar_file

from google.cloud import storage


def load_data(args, device):
    """Loads the data into three different data loaders. (Train, Test, Evaluation)
        Split the training dataset into a train / test dataset.

        Args:
            args: arguments passed to the python script
            device: PyTorch device on which to load the dataset
            client: GCP client
    """

    # Download data
    print("======= Downloading images tar file =======")
    download_bucket_file(bucket_path=IMAGES_TAR_FILE_BUCKET_PATH,
                         local_path=IMAGES_TAR_FILE_LOCAL_PATH,
                         bucket_name=BUCKET_NAME)

    print("======= Extracting images =======")
    images_path = extract_tar_file(path=IMAGES_TAR_FILE_LOCAL_PATH,
                                   dest_folder=EXTRACTION_DEST_LOCAL_FOLDER)
    images_path = str(images_path)
    print(images_path)

    print("======= Preprocessing train set images =======")
    train_filter_agent = PreprocessData(caption_meta_path=TRAIN_CAPTION_META_PATH,
                                        images_dir=images_path,
                                        save_path=TRAIN_EXPLODED_SAVE_PATH,
                                        filters=FILTERS,
                                        client=client)
    cleaned_images_train = train_filter_agent.get_filtered_data()

    print("======= Preprocessing train set captions =======")
    cleaned_text_images_train = CleanTextData(data=cleaned_images_train,
                                              replace_dict=REPLACE_DICT,
                                              punct_list=PUNCT,
                                              manual_spell_map=MANUAL_SPELL_MAP)

    print("======= Preprocessing val set images =======")
    val_filter_agent = PreprocessData(caption_meta_path=VAL_CAPTION_META_PATH,
                                      images_dir=images_path,
                                      save_path=VAL_EXPLODED_SAVE_PATH,
                                      filters=FILTERS,
                                      client=client)
    cleaned_images_val = val_filter_agent.get_filtered_data()

    print("======= Preprocessing val set captions =======")
    cleaned_text_images_val = CleanTextData(data=cleaned_images_val,
                                            replace_dict=REPLACE_DICT,
                                            punct_list=PUNCT,
                                            manual_spell_map=MANUAL_SPELL_MAP)

    # Get sample of train and val data in order to check training pipeline
    filtered_cap_dress_train = cleaned_text_images_train.filtered_data
    filtered_cap_dress_val = cleaned_text_images_val.filtered_data
    filtered_cap_dress_val = filtered_cap_dress_val[:2000]

    # Initialize BERT tokenizer
    print("======= Creatng vocabulary =======")
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)

    # Initialize Vocab
    vocab_train = Vocab(captions=filtered_cap_dress_train, tokenizer=tokenizer)
    vocab_val = Vocab(captions=filtered_cap_dress_val, tokenizer=tokenizer)
    vocab = vocab_train + vocab_val

    # Initialize Dataset
    transforms_list = transforms.Compose([transforms.ToPILImage(),
                                          transforms.Resize((224, 224)),
                                          transforms.ToTensor(),
                                          transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                               std=[0.229, 0.224, 0.225])])

    print("======= Creating train Dataset object =======")
    train_rel_cap_dataset = RelativeCaptioningDataset(list_ids=filtered_cap_dress_train,
                                                      images_dir=images_path,
                                                      transform=transforms_list,
                                                      client=client)

    print("======= Creating val Dataset object =======")
    val_rel_cap_dataset = RelativeCaptioningDataset(list_ids=filtered_cap_dress_val,
                                                    images_dir=images_path,
                                                    transform=transforms_list,
                                                    client=client)

    # Initialize Dataloader
    collate_fn = MyCollator(tokenizer=tokenizer)

    print("======= Creating train Dataloader object =======")
    train_generator_params = {'batch_size': args.batch_size,
                              'shuffle': args.shuffle_train,
                              'num_workers': args.num_workers}

    print("======= Creating val Dataloader object =======")
    val_generator_params = {'batch_size': args.batch_size,
                            'shuffle': False,
                            'num_workers': args.num_workers}

    train_iterator = DataLoader(dataset=train_rel_cap_dataset, collate_fn=collate_fn, **train_generator_params)
    val_iterator = DataLoader(dataset=val_rel_cap_dataset, collate_fn=collate_fn, **val_generator_params)

    return train_iterator, val_iterator, vocab


def save_model(args):
    """Saves the model to Google Cloud Storage

    Args:
      args: contains name for saved model.
    """
    scheme = 'gs://'
    bucket_name = args.job_dir[len(scheme):].split('/')[0]

    prefix = '{}{}/'.format(scheme, bucket_name)
    bucket_path = args.job_dir[len(prefix):].rstrip('/')

    datetime_ = datetime.datetime.now().strftime('model_%Y%m%d_%H%M%S')

    if bucket_path:
        model_path = '{}/{}/{}'.format(bucket_path, datetime_, args.model_name)
    else:
        model_path = '{}/{}'.format(datetime_, args.model_name)

    bucket = storage.Client().bucket(bucket_name)
    blob = bucket.blob(model_path)
    blob.upload_from_filename(args.model_name)
