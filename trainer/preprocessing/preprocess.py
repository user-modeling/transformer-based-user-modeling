import copy
from spellchecker import SpellChecker
from trainer.utils.helpers import load_jsonl, append_to_jsonl, load_json


class PreprocessData:
    def __init__(self, caption_meta_path, images_dir, save_path, filters, client=None):
        self.caption_metadata_path = caption_meta_path
        self.images_dir = images_dir
        self.save_path = save_path
        self.filters = filters
        self.filtered_cap_metadata = []
        self.client = client

        # Load already processed captions if exist
        if not self._load_captions_if_exist():
            print("Filtering captions...")

            # Load captions metadata
            self.cap_metadata = load_json(self.caption_metadata_path, client=self.client)

            # Explode captions metadata
            self.cap_metadata_exploded = self._explode_captions_metadata()

            # Get filtered data
            self.filtered_cap_metadata = self._filter_images()

            # Save filtered result
            self._save_filtered_cap_meta()
            print("Captions filtered")

    def _load_captions_if_exist(self):
        self.filtered_cap_metadata = load_jsonl(path=self.save_path, client=self.client)
        if self.filtered_cap_metadata:
            print("Captions already filtered")
            return True
        else:
            print("Captions not filtered")
            return False

    def _explode_captions_metadata(self):
        # Explode captions metadata
        cap_dress_exploded = []
        for cap in self.cap_metadata:
            captions_list = cap['captions']
            for caption in captions_list:
                new_cap = copy.deepcopy(cap)
                new_cap.pop('captions')
                new_cap['caption'] = caption
                cap_dress_exploded.append(new_cap)

        print(f"Captions metadata exploaded. Length of exploded data: {len(cap_dress_exploded)}")
        return cap_dress_exploded

    def _filter_image(self, target, candidate):

        # Get images paths
        target_img_path = self.images_dir + target + ".jpg"
        candidate_img_path = self.images_dir + candidate + ".jpg"

        # Check filters that determine whether sample should be discarded
        for filter_func in self.filters:
            if filter_func(trg_path=target_img_path,
                           cand_path=candidate_img_path):
                return False

        return True

    def _filter_images(self):
        filtered_cap_images = []
        for sample in self.cap_metadata_exploded:
            tr, cn, cap = sample['target'], sample['candidate'], sample['caption']
            # print (tr, cn, filters)
            if self._filter_image(target=tr, candidate=cn):
                filtered_cap_images.append(sample)

        print(f"Captions metadata filtered. Length of filtered data: {len(filtered_cap_images)}")
        return filtered_cap_images

    def _save_filtered_cap_meta(self):
        append_to_jsonl(path=self.save_path, data=self.filtered_cap_metadata, client=self.client)
        print(f"Captions metadata saved to {self.save_path}")

    def get_filtered_data(self):
        return self.filtered_cap_metadata


class CleanTextData:
    def __init__(self, data, replace_dict, punct_list, manual_spell_map):
        self.data = copy.deepcopy(data)
        self.replace_dict = replace_dict
        self.punct_list = punct_list
        self.manual_spell_map = manual_spell_map
        self.spell = SpellChecker()

        # Set up filters up to split into 2
        self.filter_list = [self._remove_n_word_caps,
                            self._replace_list,
                            self._split_into_2_if_dot]

        self.filtered_data = self.apply_filters(self.data)

        # Set up rest of teh filters
        self.filter_list = [self._spellcheck,
                            self._remove_punct_list]
        self.filtered_data = self.apply_filters(self.filtered_data)

        # Discard all None
        self.filtered_data = list(filter(None, self.filtered_data))

    def apply_filters(self, data):
        filtered_list = []
        for sample in data:
            for ff in self.filter_list:
                if sample:
                    sample = ff(sample)

            if type(sample) == list:
                filtered_list += sample
            else:
                filtered_list.append(sample)
        return filtered_list

    @staticmethod
    def _remove_n_word_caps(sample):
        caption = sample['caption']
        if not caption or len(caption.split()) == 1:
            return None
        else:
            return sample

    def _replace_list(self, sample):
        cap = sample['caption']
        for a, b in self.replace_dict.items():
            cap = cap.replace(a, b)
        sample['caption'] = cap
        return sample

    @staticmethod
    def _split_into_2_if_dot(sample):
        caption = sample['caption']
        # if len(caption.split(".")) > 1:
        # print(caption)
        caption_splitted = list(filter(None, caption.split(".")))

        if len(caption_splitted) == 1:
            sample['caption'] = caption_splitted[0]
            return sample
        else:
            sample_list = []
            for cap in caption_splitted:
                sample_tmp = copy.deepcopy(sample)
                sample_tmp['caption'] = cap
                sample_list.append(sample_tmp)
            return sample_list

    def _spellcheck(self, sample):
        caption = sample['caption']
        caption_splitted = caption.split()

        # Initial manual spellcheck
        caption_splitted = " ".join([self.manual_spell_map.get(word) if word in self.manual_spell_map
                                     else word for word in caption_splitted]).split()

        # Find those words that may be misspelled
        misspelled = self.spell.unknown([word for word in caption_splitted if "-" not in word and len(word) != 1])

        # Correct spelling mistakes and form corrected caption
        correction_dict = {}
        if misspelled:
            for word in misspelled:
                # Get the one `most likely` answer
                if word in self.manual_spell_map:
                    correction_dict[word] = self.manual_spell_map[word]
                else:
                    correction_dict[word] = self.spell.correction(word)

            corrected_caption = " ".join([correction_dict.get(word) if word in correction_dict else word
                                          for word in caption_splitted])
            print(caption)
            print(corrected_caption)
            print("=====================================================================")
        else:
            corrected_caption = caption

        sample['caption'] = corrected_caption
        return sample

    def _remove_punct_list(self, sample):
        caption = sample['caption']
        filtered_caption = "".join([char for char in caption if char not in self.punct_list])
        sample['caption'] = filtered_caption
        return sample


