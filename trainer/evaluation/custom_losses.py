import torch.nn as nn
import torch.nn.functional as F
# based on https://medium.com/towards-artificial-intelligence/how-to-use-label-smoothing-for-regularization-aa349f7f1dbb


def linear_combination(x, y, epsilon):
    return epsilon * x + (1 - epsilon) * y


def reduce_loss(loss, reduction='mean'):
    return loss.mean() if reduction == 'mean' else loss.sum() if reduction == 'sum' else loss


class CrossEntropyLossSmooth(nn.CrossEntropyLoss):
    """CrossEntropyLoss with label smoothing"""

    def __init__(self, weight=None, ignore_index=-100, reduction='mean', smooth_eps=None):
        super().__init__(weight=weight, ignore_index=ignore_index, reduction=reduction)
        self.epsilon = smooth_eps
        self.reduction = reduction
        self.ignore_index = ignore_index

    def forward(self, output, captions_batch):
        # Ignore index
        target = captions_batch[captions_batch != self.ignore_index]
        preds = output[captions_batch != self.ignore_index]
        n = preds.size()[-1]

        # Compute loss with Label Smoothing
        log_preds = F.log_softmax(preds, dim=-1)
        loss = reduce_loss(-log_preds.sum(dim=-1), self.reduction)
        nll = F.nll_loss(log_preds, target, reduction=self.reduction)
        print(loss.shape, nll.shape)
        return linear_combination(loss / n, nll, self.epsilon)
